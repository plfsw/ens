#!/bin/sh

NC=0 ; NPY=0
for fich in $(ls)
do
    case "$fich" in
        "*.py") echo "prog python : $fich" ; NPY=$((NPY+1)) ;;
        "*.c")  echo "source C : $fich"
                NC=$((NC+1)) ;;
        *)      echo "autre : $fich" ;;
    esac
done
echo "Il y a $NC sources C et $NPY programmes Python"

exit 0
